<?php

/*
Plugin Name: Stacked Brands Action Box
Plugin URI: http://www.stackedbrands.com
Description: A plugin to generate custom action codes and insert into frontend using shortcodes
Author: Stacked Brands
Author URI: http://www.stackedbrands.com
Version: 1.3.0
License: GNU
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'init', function() {

	/** Define the Namespace for options */
	define( 'THEMEPREFIX', 'sb-actionbox' );

	include __DIR__ . '/admin-page.php';
	include __DIR__ . '/options.php';

	new WhitelabelOptions( 'Stacked Brands', 'sb-actionbox-options', THEMEPREFIX, null, 'dashicons-vault', 'read', null, true, 'SB Actionboxes', true, $options, plugins_url(), 'admin' );

});
