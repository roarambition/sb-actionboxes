<?php

	$contentFloat = (get_option(THEMEPREFIX.'_global_layout') == 3 ) ? 'f-right' : 'f-left';

	$globalDropShadowHex = get_option(THEMEPREFIX."_global_color_shadow");
	list($r, $g, $b) = sscanf($globalDropShadowHex, "#%02x%02x%02x");

	$globalDropShadowSize = get_option(THEMEPREFIX."_global_shadow_size");

	$globalDropShadowCss = "-webkit-box-shadow: {$globalDropShadowSize}px {$globalDropShadowSize}px {$globalDropShadowSize}px 0px rgba({$r}, {$g}, {$b}, 0.6);-moz-box-shadow: {$globalDropShadowSize}px {$globalDropShadowSize}px {$globalDropShadowSize}px 0px rgba({$r}, {$g}, {$b}, 0.6); box-shadow:{$globalDropShadowSize}px {$globalDropShadowSize}px {$globalDropShadowSize}px 0px rgba({$r}, {$g}, {$b}, 0.6);";

	// Border CSS
	$globalBorderColor = get_option(THEMEPREFIX."_global_color_border");
	$globalBorderSize = get_option(THEMEPREFIX."_global_border_size");
	$globalBorderColorCss = "border: {$globalBorderSize}px solid {$globalBorderColor};";

	// Border Radius
	$globalBorderRadius = get_option(THEMEPREFIX."_global_border_radius");
	$globalBorderCss = "border-radius: {$globalBorderRadius}px;";

	// Background Color
	$globalBackgroundColor = get_option(THEMEPREFIX."_global_color_background");
	$globalBackgroundCss = "background: {$globalBackgroundColor};";

	// Responsive Css
	$globalFixedWidth = get_option(THEMEPREFIX."_global_fixed_width");

	// Additioanl Generic Products
	$additional_generic_products_total = get_option( THEMEPREFIX . "_global_additional_products" );
	$additional_generic_products       = [];

	for ( $i=0; $i < $additional_generic_products_total; $i++ ) {
		$human_readable_number = $i + 1;
		$pretty_name = "Product {$human_readable_number}";
		$additional_generic_products[$pretty_name] = $i;
	}

	if (get_option(THEMEPREFIX."_global_responsive_switch")):
		$responsiveCss = " _1c2 m1c1";
		$responsiveStyle = "width: auto; max-width: {$globalFixedWidth}px";
		$imgResponsive = "width: 100%;";
	else:
		$responsiveCss = " _1c2";
		$responsiveStyle = "width: {$globalFixedWidth}px";
		$imgResponsive = "width: auto;";
	endif;

	// Font styling
	$globalHeaderFontSize = get_option(THEMEPREFIX."_global_header_font_size");
	$globalHeaderFontStyle = get_option(THEMEPREFIX."_global_header_font_style");
	$globalHeaderFontColor = get_option(THEMEPREFIX."_global_color_header");
	$globalHeaderFontCss = 'style="font-style: '. $globalHeaderFontStyle .'; font-size: '. $globalHeaderFontSize .'px;color: '. $globalHeaderFontColor .';"';

	$globalTextFontSize = get_option(THEMEPREFIX."_global_text_font_size");
	$globalTextFontStyle = get_option(THEMEPREFIX."_global_text_font_style");
	$globalTextFontColor = get_option(THEMEPREFIX."_global_color_text");
	$globalTextFontCss = 'style="font-style: '. $globalTextFontStyle .'; font-size: '. $globalTextFontSize .'px;color: '. $globalTextFontColor .';"';
	$globalLinkFontColor = get_option(THEMEPREFIX."_global_class_link");
	$globalLinkFontCss = 'class="'. $globalLinkFontColor .';"';

	// Classic Font List
	$font_list = array('Arial, sans-serif', 'Verdana, sans-serif', 'Tahoma, sans-serif', 'Georgia, serif');


/* 	File Paths
	================================================= */
	$currentDir = plugin_dir_url( __FILE__ );

	$imgurl = $currentDir . 'images/';

	//Access the WordPress Pages via an Array
	$pages = array();
	$pages_obj = get_pages('sort_column=post_parent,menu_order');
	foreach ( $pages_obj as $key ) {
		$pages[$key->ID] = ucwords($key->post_title);
	}


	//Access the WordPress Pages via an Array
	$tags = array();
	$tags_obj = get_tags('orderby=name&hide_empty=false&get=all');
	foreach ( $tags_obj as $key ) {
		$tags[$key->term_id ] = ucwords($key->name);
	}


	//Access the WordPress Categories via an Array
	$categories = array();
	$categories_obj = get_categories('hide_empty=0');
	foreach ( $categories_obj as $key ) {
		$categories[$key->cat_ID] = ucwords($key->cat_name);
	}

		// We generate the three sections of HTML used in the preview div
	function get_previewbox_html( $product ) {

		global $responsiveCss;
		global $contentFloat;
		global $responsiveStyle;
		global $globalBorderCss;
		global $globalTextFontCss;
		global $globalLinkFontCss;
		global $globalHeaderFontCss;
		global $globalDropShadowCss;
		global $globalBackgroundCss;
		global $globalBorderColorCss;

		$productImg = (get_option(THEMEPREFIX.'_'. $product .'_custom_image')) ? get_option(THEMEPREFIX.'_'. $product .'_custom_image') : get_option(THEMEPREFIX.'_'. $product .'_image');

		$sbactionHtml =	'<div class="box-wrapper sb_actionboxes" style="'. $globalDropShadowCss . $globalBorderColorCss . $globalBorderCss . $globalBackgroundCss . $responsiveStyle .'">';

		if (get_option(THEMEPREFIX.'_global_layout') == 1 ):
			$sbactionHtml .= '<div class="sb_header centered-text mtop10" '. $globalHeaderFontCss .'>'. get_option(THEMEPREFIX."_". $product ."_header_text") .'</div>';
		endif;

		$sbactionHtml .= '<div class="sb_content group">';

		$sbactionHtml .= '<div class="'. $contentFloat . $responsiveCss .'">';

		if (get_option(THEMEPREFIX.'_global_layout') != 1 ):
			$sbactionHtml .= '<div class="sb_header mtop10" '. $globalHeaderFontCss .'>'. get_option(THEMEPREFIX."_". $product ."_header_text") .'</div>';
		endif;

		$sbactionHtml .= '<div class="sb_text" '.$globalTextFontCss.'>'. wpautop(get_option(THEMEPREFIX."_". $product ."_main_text")) .'<a '. $globalLinkFontCss .'href="'. get_option(THEMEPREFIX."_". $product ."_link_url") .'">'. get_option(THEMEPREFIX."_". $product ."_link_text") .'</a></div>';
		$sbactionHtml .= '</div>';

		$sbactionHtml .= '<div class="'. $contentFloat . $responsiveCss .'">';
		$sbactionHtml .= '<div class="sb_image"><a href="'. get_option(THEMEPREFIX."_". $product ."_link_url") .'"><img class="mleft20" src="'. $productImg .'" /></a></div>';
		$sbactionHtml .= '</div>';
		$sbactionHtml .= '</div>';
		$sbactionHtml .= '</div>';

		return $sbactionHtml;

	}

	function get_default_images( $product ) {

		$default_images = [];
		$img_path 		= dirname( __FILE__ ) . '/images';
		$img_url 		= plugin_dir_url( __FILE__  ) . 'images/';

		// Get all matching product images.
		// Uses image path as glob doesn't work with URLs
		$image_paths = glob( $img_path . "/{$product}*.*" );

		foreach ( $image_paths as $image_path ) {
			// Grab the filename from the fullpath
			$image_filename       = basename( $image_path );
			// Prepare the url ready for use in the browser.
			$img                  = $img_url . $image_filename;
			$default_images[$img] = $img;
		}

		// return $default_images;
		return $default_images;
	}

	function sb_options_generator( $pretty_name, $product, $categories ) {

		$product_preview_html = get_previewbox_html( $product );

			$options[] = array( "name" => __( $pretty_name,'stackedbrands'),
								"type" => "section");

			$options[] = array( "name" => __('Configure your action boxes','stackedbrands'),
								"type" => "message",
								"std" => "<p>Use this tab to configure your {pretty_name} action box. You have the option of images on white, black and transparent backgrounds</p>");

			$options[] = array( "name" => __("{$pretty_name} Box Preview",'stackedbrands'),
								"type" => "html",
								"std" => "<div id='preview-section-$product' class='preview-actionbox'>{$product_preview_html}</div>");

			// divider
			$options[] = array("type" => "divider");

			$options[] = array( "name" => __("Custom Image","stackedbrands"),
						"desc" => __("<p>If you want to upload your own image instead of using a stock image you can do it here.</p><p>For best results crop your image to <strong>240px by 240px</strong> in size and save at 70% quality.</p>", "stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_custom_image",
						"class" => "medium first",
						"std" => null,
						"type" => "upload");

			$options[] = array(
						"name" => __("Standard Image","stackedbrands"),
						"desc" => __("{$pretty_name} Standard Image", "stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_image",
						"class" => "medium last sb-img-thumb",
						"std" => null,
						"type" => "images",
						"options" => get_default_images( $product ),
						);

			// divider
			$options[] = array("type" => "divider");

			// Header Text Field
			$options[] = array( "name" => __("Header Text","stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_header_text",
						"class" => "medium first",
						"placeholder" => "Add the action box header",
						"std" => null,
						"type" => "text");

			// Main Textarea Field
			$options[] = array( "name" => __("Main Text","stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_main_text",
						"class" => "medium second",
						"placeholder" => "Add the main text",
						"std" => null,
						"type" => "textarea");

			// divider
			$options[] = array("type" => "divider");

			// Link Text Field
			$options[] = array( "name" => __("Link Text","stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_link_text",
						"class" => "medium first",
						"placeholder" => "Add the CTA link text",
						"std" => null,
						"type" => "text");

			// Link Url Field
			$options[] = array( "name" => __("Link Url","stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_link_url",
						"class" => "medium second",
						"placeholder" => "Add the CTA link url",
						"std" => null,
						"type" => "text");

			// divider
			$options[] = array("type" => "divider");

			// Get post types, page and posts

			//
			// Note, this needs amended to fire later to pick up custom post types
			// http://wordpress.stackexchange.com/questions/100406/get-post-types-is-not-showing-all-registered-posts
			//
			$posttypeArgs = array(
		        'public'                => true
		    );

		    $output = 'names'; // names or objects, note names is the default
		    $operator = 'and'; // 'and' or 'or'
		    $post_types = get_post_types($posttypeArgs,$output,$operator);

		    $posttypeFiltered = array_diff($post_types, array( 'attachment', 'nav_menu', 'nav_menu_item', 'revision'));

			//$posttypeNumerical = array_values($posttypeFiltered);

			// Multi-Select - Post Types to add this action box to
			$options[] = array( "name" => __("Post Types","stackedbrands"),
						"desc" => __("Click the white field and add all the post types you want this action box added to", "stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_posttypes",
						"class" => "medium first",
						"std" => null,
						"type" => "select_multiple",
						"options" => $posttypeFiltered);



			// Multi-Select - Categories to add this action to
			$options[] = array( "name" => __("Categories","stackedbrands"),
						"desc" => __("Add categories you want this action box added to - if you select 'post' in the Post Type field it will be added", "stackedbrands"),
						"id" => THEMEPREFIX."_{$product}_cattypes",
						"class" => "medium second",
						"std" => null,
						"type" => "select_multiple",
						"options" => $categories);

			return $options;
	}


/*	Start Admin Options
	================================================= */
	$options = array();


/*	Welcome
	================================================= */

	$options[] = array( "name" => __('About','stackedbrands'),
						"type" => "section");

	// Main Logo
	$options[] = array( "type" => "html",
						"std" => "<img class='mob-responsive' src='{$imgurl}sb-action-logo.png' />");


	// Welcome Messages
	$options[] = array( "name" => __('Configure your action boxes','stackedbrands'),
						"type" => "message",
						"std" => '<p>This Wordpress admin panel allows you to build your action boxes.  Use the tabs on the left hand side to build one for each product.  You can choose pre-selected images or upload your own.</p><p>Add your own headings, text, and links. You can apply a variety of styles to ensure the action boxes fit the style of your website and look individual.  As you go along the action boxes will be rendered at the top of each tab every time you save your settings.</p><p>This allows you to tune your work for maximum conversion optimization.</p>');

	$options[] = array( "name" => __('Save your action boxes','stackedbrands'),
						"type" => "message",
						"std" => '<p>Save your work as you go along, and create a backup of your styles and settings. You can experiment with new styles and reload saved styles from backup if they don\'t work out.</p></p>Use the Shortcodes buttons in the visual editor of posts and pages to insert the action boxes.</p>');

	$options[] = array( "name" => __('Publish your action boxes','stackedbrands'),
						"type" => "message",
						"std" => '<p>Once you are happy with the configurations you have created you can use the custom shortcodes in the visual editor of posts and pages to insert the action boxes in the best positions.</p><p>You can also add them via widgets or check the general settings to apply them automatically at the foot of the content in specific categories or pages.</p>');


/*	Global Styling Options
	================================================= */

	$options[] = array( "name" => __('Styling Options','stackedbrands'),
						"type" => "section");

	// TODO - When the individual styling over rides are done in V2;

	// Welcome Message
	//$options[] = array( "name" => __('Set your styling options','stackedbrands'),
	//					"type" => "message",
	//					"std" => '');


	//<p>You are able to set and override the styles of action boxes on a product by product level (ie TestoFuel is in a square blue box, Instant Knockout is in a white rounded box</p><p>However the chances are that you want the styling the same for all the action boxes depending on the style of your theme.</p><p>You can set the styles for all action boxes in this tab and over-ride them in the individual products if you want to.</p>


	// Render
	$options[] = array( "name" => __('How your Action Boxes Will Look','stackedbrands'),
						"type" => "title");

	$options[] = array( "name" => __('Note: This is just an example','stackedbrands'),
						"type" => "message",
						"std" => '<p>You will be able to set your own text and the images on the individual product pages.</p>');

	$previewHtml =	'<div class="box-wrapper sb_actionboxes" style="'. $globalDropShadowCss . $globalBorderColorCss . $globalBorderCss . $globalBackgroundCss . $responsiveStyle .'">';

	if (get_option(THEMEPREFIX.'_global_layout') == 1 ):
		$previewHtml .= '<div class="sb_header centered-text mtop10" '. $globalHeaderFontCss .'>Your Header</div>';
	endif;

	$previewHtml .= '<div class="sb_content group">';

	$previewHtml .=	'<div class="'. $contentFloat . $responsiveCss .'">';

	if (get_option(THEMEPREFIX.'_global_layout') != 1 ):
		$previewHtml .= '<div class="sb_header mtop10" '. $globalHeaderFontCss .'>Your Header</div>';
	endif;

	$previewHtml .= '<div class="sb_text" '.$globalTextFontCss.'><p>Lorem ipsum dolor sit amet, est ea graeci efficiendi. Noster necessitatibus sit cu, putant utamur ut sit, ei contentiones comprehensam cum. Cum nostro pericula cu, minimum dissentiet in vim. Dolore apeirian disputationi quo in, vix aeque consul ullamcorper an.</p><p>Ea iusto appetere vis. Quod quidam quaerendum usu eu, mei in dicta albucius. In nemore elaboraret consectetuer qui, in omnes patrioque pri, nemore nusquam repudiandae his no. <a '. $globalLinkFontCss .'href="http://www.stackedbrands.com">Your link here</a></p></div>';
	$previewHtml .= '</div>';

	$previewHtml .= '<div class="'. $contentFloat . $responsiveCss .'">';
	$previewHtml .= '<div class="sb_image"><img style="'. $imgResponsive .'" src="'. $imgurl .'layout-demo-logo.png" /></div>';
	$previewHtml .= '</div>';
	$previewHtml .= '</div>';
	$previewHtml .= '</div>';

	// The render
	$options[] = array( "type" => "html",
						"std" => "<div id='preview-section-global' class='sb-actionbox'>{$previewHtml}</div>");

	// divider
	$options[] = array("type" => "divider");

	// Title
	$options[] = array( "name" => __('Layout','stackedbrands'),
						"type" => "title");

	// Images
	$options[] = array( "name" => __("Layout Option","stackedbrands"),
				"desc" => __("Choose the default layout option", "stackedbrands"),
				"id" => THEMEPREFIX."_global_layout",
				"class" => "",
				"std" => 2,
				"type" => "images",
				"options" => array(
					1 => $imgurl.'layout-1.png',
					2 => $imgurl.'layout-2.png',
					3 => $imgurl.'layout-3.png',
				));

	// Additional Generic Products
	$options[] = array(
				'name' => __( 'Additional Products','stackedbrands' ),
				'desc' => __( 'Select the number of additional products', 'stackedbrands' ),
				'id' => THEMEPREFIX . '_global_additional_products',
				'class' => '',
				"std" => 0,
				"min" => 0,
				"max" => 10,
				"increment" => 1,
				"type" => "number",
			);


	// divider
	$options[] = array("type" => "divider");

	// Title
	$options[] = array( "name" => __('Responsive or Fixed','stackedbrands'),
						"type" => "title");

	// Switch Field
	$options[] = array( "name" => __("Responsive Switch","stackedbrands"),
				"desc" => __("Turn on for responsive, off for fixed width", "stackedbrands"),
				"id" => THEMEPREFIX."_global_responsive_switch",
				"class" => "medium first",
				"std" => 'true',
				"type" => "switch");

	// Slider - Global border radius
	$options[] = array( "name" => __("Fixed/Max Width (pixels)","stackedbrands"),
				"desc" => __("If you have chosen fixed width set the width here. If you have chosen responsive you can use this to set a maximum width.", "stackedbrands"),
				"id" => THEMEPREFIX."_global_fixed_width",
				"class" => "short second",
				"std" => 640,
				"min" => 200,
				"max" => 1000,
				"suffix" => "px",
				"increment" => 10,
				"type" => "number");

	// divider
	$options[] = array("type" => "divider");

	// Title
	$options[] = array( "name" => __('Colors','stackedbrands'),
						"type" => "title");

	// Color - Global Background
	$options[] = array( "name" => __("Background Color","stackedbrands"),
				"desc" => __("The background color of the action boxes", "stackedbrands"),
				"id" => THEMEPREFIX."_global_color_background",
				"class" => "short first",
				"std" => '#ffffff',
				"type" => "color");

	// Color - Global border
	$options[] = array( "name" => __("Border Color","stackedbrands"),
				"desc" => __("The border color of the action boxes", "stackedbrands"),
				"id" => THEMEPREFIX."_global_color_border",
				"class" => "short second",
				"std" => '#cccccc',
				"type" => "color");

	// Color - Global shadow
	$options[] = array( "name" => __("Shadow Color","stackedbrands"),
				"desc" => __("The shadow color of the action boxes", "stackedbrands"),
				"id" => THEMEPREFIX."_global_color_shadow",
				"class" => "short third",
				"std" => '#474747',
				"type" => "color");

	// Color - Global header
	$options[] = array( "name" => __("Header Color","stackedbrands"),
				"desc" => __("The header color of the action boxes", "stackedbrands"),
				"id" => THEMEPREFIX."_global_color_header",
				"class" => "short first",
				"std" => '#000000',
				"type" => "color");

	// Color - Global text
	$options[] = array( "name" => __("Text Color","stackedbrands"),
				"desc" => __("The text color of the action boxes", "stackedbrands"),
				"id" => THEMEPREFIX."_global_color_text",
				"class" => "short second",
				"std" => '#333333',
				"type" => "color");

	// Color - Global link
	$options[] = array( "name" => __("Link Class","stackedbrands"),
				"desc" => __("The link CSS class of the action boxes", "stackedbrands"),
				"id" => THEMEPREFIX."_global_class_link",
				"class" => "short third",
				"std" => null,
				"type" => "text");

	// divider
	$options[] = array("type" => "divider");

	// Title
	$options[] = array( "name" => __('Styles','stackedbrands'),
						"type" => "title");

	// Slider - Global border radius
	$options[] = array( "name" => __("Corner Radius","stackedbrands"),
				"desc" => __("Increase the number to make the corners more rounded", "stackedbrands"),
				"id" => THEMEPREFIX."_global_border_radius",
				"class" => "short first",
				"std" => 2,
				"min" => 0,
				"max" => 10,
				"suffix" => "px",
				"increment" => 1,
				"type" => "number");

	// Slider - Global border radius
	$options[] = array( "name" => __("Shadow Size","stackedbrands"),
				"desc" => __("Increase the number to increase drop shadow", "stackedbrands"),
				"id" => THEMEPREFIX."_global_shadow_size",
				"class" => "short second",
				"std" => 2,
				"min" => 0,
				"max" => 10,
				"suffix" => "px",
				"increment" => 1,
				"type" => "number");

	// Slider - Global border thickness
	$options[] = array( "name" => __("Border Thickness","stackedbrands"),
				"desc" => __("The border width in pixels", "stackedbrands"),
				"id" => THEMEPREFIX."_global_border_size",
				"class" => "short third",
				"std" => 2,
				"min" => 0,
				"max" => 5,
				"suffix" => "px",
				"increment" => 1,
				"type" => "number");


	// divider
	$options[] = array("type" => "divider");

	// Title
	$options[] = array( "name" => __('Fonts','stackedbrands'),
						"type" => "title");


	$options[] = array( "name" => __("Header Font","stackedbrands"),
				"desc" => __("Set the font weight and size for the header", "stackedbrands"),
				"id" => THEMEPREFIX."_global_header_font",
				"class" => "medium first",
				"std" => array(22, 0),
				"min" => 10,
				"max" => 24,
				"suffix" => "px",
				"type" => "font",
				"options" => $font_list);

	$options[] = array( "name" => __("Text Font","stackedbrands"),
				"desc" => __("Set the font weight and size for the main text", "stackedbrands"),
				"id" => THEMEPREFIX."_global_text_font",
				"class" => "medium second",
				"std" => array(16, 0),
				"min" => 12,
				"max" => 20,
				"suffix" => "px",
				"type" => "font",
				"options" => $font_list);

	$sb_action_box_products = array(
		'4 Gauge'          => 'fourgauge',
		'TestoFuel'        => 'testofuel',
		'Prime Male'       => 'primemale',
		'Instant Knockout' => 'instantknockout',
	);

	$sb_action_box_products = array_merge( $sb_action_box_products, $additional_generic_products );


	// Loop over all products passed in with initial call of function
	foreach ( $sb_action_box_products as $pretty_name => $product ) {
		$options = array_merge( $options, sb_options_generator( $pretty_name, $product, $categories ) );
	}
