(function() {

	function return_menu( editor ) {

		var defaultMenu = [

				/** TestoFuel Start **/
				{
				text: 'TestoFuel',
				menu: [

						/* Accordion */
						{
							text: 'Standard Actionbox',
							onclick: function() {
								editor.insertContent( '[testofuel_ab]');
							}
						}, // End accordion


					]
				}, // End jQuery section

				/** 4 Guage Start **/
				{
				text: '4 Gauge',
				menu: [

						/* Accordion */
						{
							text: 'Standard Actionbox',
							onclick: function() {
								editor.insertContent( '[fourgauge_ab]');
							}
						}, // End accordion


					]
				}, // End jQuery section

				/** PrimeMale Start **/
				{
				text: 'Instant Knockout',
				menu: [

						/* Accordion */
						{
							text: 'Standard Actionbox',
							onclick: function() {
								editor.insertContent( '[instantknockout_ab]');
							}
						}, // End accordion


					]
				}, // End jQuery section

				/** PrimeMale Start **/
				{
				text: 'Prime Male',
				menu: [

						/* Accordion */
						{
							text: 'Standard Actionbox',
							onclick: function() {
								editor.insertContent( '[primemale_ab]');
							}
						}, // End accordion


					]
				}, // End jQuery section

				/* Video */
				{
					text: 'Responsive Video',
					onclick: function() {
						editor.windowManager.open( {
							title: 'Insert YouTube Embed Video ID',
							body: [ {
								type: 'textbox',
								name: 'responsiveVideoId',
								label: 'YouTube video ID',
								value: '',
								tooltip: 'ie UQQ-ERH76Yw'
							},
																// Button URL
			                {
			                    type   : 'container',
			                    name   : 'container',
			                    label  : 'Insert the ID only',
			                    html   : 'ie <span style="text-decoration: line-through">https://www.youtube.com/watch?v=</span><span style="font-weight:bold;color: #d32020">UQQ-ERH76Yw</span>'
			                }, ],
							onsubmit: function( e ) {
								editor.insertContent( '<div class="sbvideoWrapper"><div class="sbvideoContainer">[embed]https://www.youtube.com/embed/' + e.data.responsiveVideoId + '[/embed]</div></div>');
							}
						});
					}
				} // End Video

			];

			var additionalMenu = [];
			// Uses localized variable enqueued in admin-page.php
			var length = sb_additional_products;
			for (var i = 0; i < length; i++) {
				(function(index) {
						humanReadableNumber = index + 1;
				       	formattedName = 'Product ' + humanReadableNumber;
				           additionalMenu.push({
				               text: formattedName,
				               menu: [
				               		{
				               			text: 'Standard Actionbox',
				               			onclick: function() {
				               				editor.insertContent( '[' + index + '_ab]');
				               			}
				               		},
				               	]
				           });
				   })(i);
			}

		return defaultMenu.concat(additionalMenu);
	}

	tinymce.PluginManager.add( 'sb_actionbox_mce_button', function( editor, url ) {
		editor.addButton( 'sb_actionbox_mce_button', {
			title: 'Sb Actionbox',
			type: 'menubutton',
			icon: 'icon sb-actionbox-icon',
			menu: return_menu( editor ),
		});
	});
})();